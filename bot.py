import os
import random

import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='!')

@bot.event
async def on_ready():
    print(f'{bot.user} has connected to Discord!')

@bot.command(name='roll')
async def roll(ctx):
    roll_value = random.randint(1, 99)
    msg = ""
    if roll_value < 10:
        msg += "渣架!"
    elif roll_value < 50:
        msg += "咁都想同大sora搶裝?"

    if roll_value == 98:
        msg += "我!98!"
    await ctx.send("Need {}! {}".format(roll_value, msg))

bot.run(TOKEN)